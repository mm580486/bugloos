import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public mainForm: FormGroup;
  public newFieldForm: FormGroup;
  public formLoader = false; // Anmiation Loader
  namePattern = "^[a-zA-Z0-9]$"; // only character and numver accept
  fields: any = [{name: 'username',value: '',field_type: 'number',pattern: '^[a-z0-9_-]{8,15}$'}];

  constructor( public modalService: NgxSmartModalService, private formBuilder: FormBuilder){
    this.mainForm = this.formBuilder.group({
      'customFields': this.formBuilder.array([ ])
    });


    this.newFieldForm = this.formBuilder.group({
      'name': ['',Validators.required],
      'value': [],
      'required': [false],
      'pattern': [null,Validators.required],
      'field_type': [null,Validators.required]
    });

    // Load Fake data
    this.loadDynamicFields();
  }


// Add new field to form array
// @INPUT name: Display name
// @INPUT value: Default Value
// @INPUT field_type: Field type like text field textarea etc
// @INPUT pattern: Regex pattern for validate value
// @OUTPUT createItem: Single form array for use on push methods
  createItem(name = '', value= '',field_type='',pattern = '',required = false): FormGroup {
    return this.formBuilder.group({
      name: name,
      field_type: field_type,
      value: [value,Validators.pattern(pattern)],
      required: required
    });
  }

// Get stored customeField in form builder (used on view)
// @OUTPUT Array of fields
  get getCustomFields() {
     return this.mainForm.get('customFields') as FormArray;
  }

  // Load Fake Data
  loadDynamicFields(){
    const formArray = this.mainForm.get('customFields') as FormArray;
    this.fields.forEach((value: any) => {
      formArray.push(this.createItem(value.name,'',value.field_type,value.pattern,value.required));
    });

  }

// Submit Action on newFieldModal
  addField(){
    const formArray = this.mainForm.get('customFields') as FormArray;
    formArray.push(
      this.createItem(
        this.newFieldForm.get('name')?.value,
        this.newFieldForm.get('value')?.value,
        this.newFieldForm.get('field_type')?.value,
        this.newFieldForm.get('pattern')?.value,
        this.newFieldForm.get('required')?.value
        )
        );
        this.modalService.getModal('newFieldModal').close();
  }

// Remove single field with index 
  removeWithIndex(i = 0) {
    const formArray = this.mainForm.get('customFields') as FormArray;
    formArray.removeAt(i);
  }
  // MaingForm Log
  submitForm() {
    console.log(this.mainForm.value);
  }
}
